 ## [Weekly Report](https://gitlab.com/daniairdina/dania/-/blob/main/Weekly_Report.md) 


## INTRODUCTION

<img src="https://gitlab.com/198602/dania/-/raw/main/file/WhatsApp_Image_2021-10-28_at_22.27.48.jpeg" width=150 align=middle>

### Biography

* *NAME:* Dania Irdina Akmal Binti Akmal 

* *AGE:* 22

* *MATRIC NO:* 198602

* *COURSE:* Bachelor Of Aerospace Engineering  With Honors

## Strength & Weakness

| Strength | Weakness |
| :----------: | :-----------: |
| creative | procrastinate  |
| positive | shy | 
| nice | get distracted quickly |

