<div align="right">

**Date: 24th December 2021**

</div>


# Agenda
- Test new motors by using power supply (22V) because if we directly connected the motors with battery we cannot control the amount of power applied to motors

<div align="center">

<img src="https://gitlab.com/daniairdina/dania/uploads/813d92b2221dc0eb17444b6594b8964e/WhatsApp_Image_2022-01-09_at_09.26.32.jpeg" height=500 >

</div>



# Goals
- Find out the performance of the new motors in terms of maximum thrust and vibrations

# Problem and task done
| Subsystem | Group |
| ---  | ---     |
| There is spark when we conducted the test at first but then we add nut as a gap to avoid the friction between motors and the holder.| No assigned role for this week |

<div align="center">

<img src="https://gitlab.com/daniairdina/dania/uploads/30c544b89526f8d9a0dccadc8d0e6905/WhatsApp_Image_2022-01-09_at_09.47.46.jpeg" width=500>

</div>

# Result

<div align="center">

**Motor Testing Data**

</div>

<div align="center">

<img src="https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/raw/main/Images_&_Videos/Motor_Load_Testing_Data.png" width=600>

</div>



Based on load testing data above and calculation, for 30% from maximum throttle with 17 inch

| Data | Value |
| :------: | ------ |
| Thrust | 1095 g |
| RPM | 3600 |
| Voltage | 340 KV |
| Current |2.3 A|


# Next step
- Analyse graph for Thrust and Vibration vs Time
- Start working on report



[ [Go to Week 8](Week 8.md) ][ [Go to Week 10](Week 10.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
