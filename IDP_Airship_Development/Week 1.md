<div align="right">

**Date: 22 October 2021**

</div>

# Agenda
- Course introduction
For the first week, all student has been introduced by Dr Salahuddin as our main instructor for this Aerospace Design Project. 
- Plan and create a [Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130) which can be accessed by everyone based on our subsytem.

# Goals
-  Make automated airship with sprayer fly

# Problem and decision taken
- There is no problem occur in this week.  
- For this week, we had discusses and roughly sketch the [Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130) for our plan throughout this semester so that we can keep track our progress and 
practice better time management.

# Impact
- Everyone from our subsytem get more clarity about this project and get know what happen in this week.

# Next step
- Complete task given
- Exhange idea with other subsystem to get better idea and understanding

[ [Go to Week 2](Week 2.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]



