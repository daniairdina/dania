<div align="right">

**Date: 29 October 2021**

</div>

# Agenda
- Course introduction
For the second week, all student has been divided into groups according to our interest and capability. There are several subsystems involved in order to make this process more efficients. The subsystems will be the group which inlvolve all final year  student which will be divided by following their capabilities and interest. It was made up of seven groupings of subsystems which are flight integration, control systems, propulsion, design, simulation, and payload (Sprayer). Then, on the right side, another team is formed based on these seven subsystems groups, which are made up of students from the subsystems groups, and the assignment for the progress report, presentation, and so on is given. The subsystem teams and team are shown in figure below.

<div align="center">

<img src="https://gitlab.com/daniairdina/dania/uploads/b9e856a32d758d006d9be644bbed9cd5/WhatsApp_Image_2021-11-12_at_16.28.25.jpeg" width=300 >

</div>

- Plan and create a [Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130) which can be accessed by everyone based on our subsytem.

# Goals
-  Make automated airship with sprayer fly

# Problem and decision taken
- There is no problem occur in this week.  
- For this week, we make proper [Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130) which can access by everyone for our plan throughout this semester so that we can keep track our progress and 
practice better time management.

# Impact
- Everyone from our subsytem get more clarity about this project and get know what happen in this week.

# Next step
- Complete task given
- Exhange idea with other subsystem to get better idea and understanding

[ [Go to Week 1](Week 1.md) ][ [Go to Week 3](Week 3.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
