<div align="right">

**Date: 5 November 2021**

</div>

# Agenda

- Propulsion Test using previous [Motor of PHSAU](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/blob/main/Task%20Report/Week%203.md)
- Present [Gantt Chart](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130) 
- Regroup in team to appoint a checker, process monitor, recorder and coordinator.
- Engineeering Logbook

| Role | Name |
| ---  | ---     |
| Coordinator | Haikal |
| Process monitor | Izham |
| Recorder | Dania |
| Checker | Kisshore |

# Goals
Propulsion Test<br>
The major goal of this thrust test experiment is to determine whether or not the motor is still operational, as well as the capabilities of the motor and the esc with a different length of propeller that has previously been employed.

# Problem and decision taken
| Subsystem | Group |
| ---  | ---     |
| 1. Difficulties in determining the diameter of the propeller used to propel the airship. As a result, we put 17-inch and 23-inch propellers on a sample motor in the propulsion lab, but the motor couldn't handle the 23-inch propeller and burned. As a result, we've decided not to deal with the 23-inch propeller any longer because it will cause damage at only approximately half throttle. Therefore, we are suggesting the propeller size is 17 inch with 75% throttle. <br>2. Need design Parameters from Flight Simulation Team | Create a proper [timetable](https://docs.google.com/spreadsheets/d/1q4PM3T5ifsz60wNch8EUkEgh1QDCVQ1IU3sXuDvTYkY/edit#gid=0) for roles for each member |


# Impact
Get idea on how it works

# Next step
Run the thrust test again with motors available in the propulsion lab which have similar specifications as T-Motor MN5212 KV340



[ [Go to Week 2](Week 2.md) ][ [Go to Week 4](Week 4.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
