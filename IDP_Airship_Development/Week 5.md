<div align="right">

**Date: 19 November 2021**

</div>

# Agenda
- Try to find drag of the airship using trolly
<div align="left">

<img src="https://gitlab.com/daniairdina/dania/uploads/9751eacce65274f4a61b1056f29ac679/Week4_1__1_.jpeg" width=300 >

</div>

- Discuss [design parameter](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/blob/main/Task%20Report/Week%204.md) obtained by Sprayer Team and Flight Simulation Team
- Test the functionality of the motors  

# Goals


# Problem and decision taken
| Subsystem | Group |
| ---  | ---     |
| 1. Try to find drag of the airship using trolly but the result obtained was not valid due to unconstant force applied <br> 2. Test the functionality of the old motors which have almost similar specification with  Tarot 4114 320KV and [data](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/blob/main/Task%20Report/Week%205.md) obtained were recorded. We got some question on thrust test but fortunately Dr Ezanee give an advise and some reference paper regarding the Airship Development for the groups | - |

# Impact
We had familiarized ourselves with the RC Benchmark system as well as the testing methods so that we can install the motors immediately onto the workbench and execute the test with few complications when new motors arrived

# Next step
Continue test the motor with battery

[ [Go to Week 4](Week 4.md) ][ [Go to Week 6](Week 6.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
