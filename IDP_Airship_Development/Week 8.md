<div align="right">

**Date: 17th December 2021**

</div>

# Agenda

- Test new motors

# Goals
- Find out the performance of the new motors in terms of maximum thrust and vibrations

# Problem and task done
| Subsystem | Group |
| ---  | ---     |
| We were told that it was not necessary to run the thrust test again for the new motors but then we have decided to also carry out the thrust test after getting confirmation with Dr Salah.| No assigned role for this week |

# Impact
- Cannot see the performance of new motors as we did not conducted the test yet.

# Next step
- Conduct the thrust test 


[ [Go to Week 7](Week 7.md) ][ [Go to Week 9](Week 9.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
