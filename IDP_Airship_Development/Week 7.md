
<div align="right">

**Date: 3rd December 2021**

</div>

# Agenda

1. Conducted a [discussion](Files/week_7_discussion_with_Dr_Ezanee.pdf) with Dr Ezanee about result obtained last week. 

<div align="center">

<img src="https://gitlab.com/daniairdina/dania/uploads/616922066bfad4c2b72858675df20694/meeting_dr_ezanee.JPG" width=600 >

</div>

Few main points that we discussed:<br>
- Options on how we want to land the airship. If we want to land airship we need to know the maximum swivel angle of the motors. <br>
- Contribution of vibration on airship
- Purpose of current relative thrust graph plotting



<div align="center">

<img src="https://gitlab.com/daniairdina/dania/uploads/467fd553e3dc97ba2a7304471a0e9857/currentvsthrust_battery15.JPG" width=600 >

</div>


# Goals
- find maximum threshold for the battery and motor performance will prevent the motors to be overworked.

# Problem and task done
| Subsystem | Group |
| ---  | ---     |
| 1. Graph for thrust relative to current was plotted.| No assigned role for this week |

# Impact
- Learn on effect on battery capacity  and how it affects endurance and range and if range of operational current and maximum current value is within range at cruise that mean we are flying within limitation based on ESC specification.


# Next step
- Conduct the thrust test  when we receive the battery and ESC from the supplier.


[ [Go to Week 6](Week 6.md) ][ [Go to Week 8](Week 8.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
