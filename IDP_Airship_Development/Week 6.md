<div align="right">

**Date: 26 November 2021**

</div>

# Agenda
Conducted test 2nd motor with power supply (16V) and also battery (15.8V) with ESC Number 4.

# Goals
Compare the thrust, current and vibration produce between power supply and battery.

# Problem and decision taken
| Subsystem | Group |
| ---  | ---     |
|1. We had some error in troubleshooting current problem due to poor connection. Initially we thought that it was the crocodile clips that were causing the problem but actually it was the loose connection on the ESC wire bt then the source of error which is from the loose connection.<br>2. From testing process, we also detect that the Wrong propeller orientation causing lots of vibration and unstable thrust, therefore choose the correct propeller with correct orientation.After all troubleshoot, we were able to obtain thrust of 1200+- gf of thrust when given 100% of throttle with the battery (15.8V) and 17inch propeller. Current was about (9A)|Ensure all member update their weekly report in gitlab|

<div align="center">

<img src="https://gitlab.com/daniairdina/dania/uploads/a4965cd146ae60e381026e84f74e851e/WhatsApp_Image_2021-11-25_at_21.55.46.jpeg" width=150 >

<img src="https://gitlab.com/daniairdina/dania/uploads/0651cba9ef2c1bbc2a626bca01f82b92/23.11.21_Prop.jpeg" width=450 >

</div>

# Impact
Vibrations and unsteady thrust are caused by incorrect propeller orientation. Therefore, we need to make sure we choose the correct propeller  and make sure it's oriented correctly during the assembly of airship in future.

# Next step
Discuss with Dr Ezanee about the result obtained and wait new motors to arrive



[ [Go to Week 5](Week 5.md) ][ [Go to Week 7](Week 7.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
