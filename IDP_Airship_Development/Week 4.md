<div align="right">

**Date: 12 November 2021**

</div>

# Agenda
- Try to find drag of the airship using trolly
<div align="left">

<img src="https://gitlab.com/daniairdina/dania/uploads/9751eacce65274f4a61b1056f29ac679/Week4_1__1_.jpeg" width=300 >

</div>

- Discuss [design parameter](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/-/blob/main/Task%20Report/Week%204.md) obtained by Sprayer Team and Flight Simulation Team
- Test the functionality of the motors  

# Goals


# Problem and decision taken
| Subsystem | Group |
| ---  | ---     |
| 1. Try to find drag of the airship using trolly but the result obtained was not valid due to unconstant force applied <br> 2. Test the functionality of the motors and data obtained were recorded. 2. We got some question on thrust test but fortunately Dr give an advise and some reference paper regarding the Airship Development for the groups | - |

# Impact
We had familiarized ourselves with the RC Benchmark system as well as the testing methods

# Next step
Continue test the motor to see effect if we use maximum throttle


[ [Go to Week 3](Week 3.md) ][ [Go to Week 5](Week 5.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]

