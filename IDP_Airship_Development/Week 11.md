<div align="right">

**Date: 2nd January 2022**

</div>


# Agenda
- Start working on report focused on result

# Goals
- Finish the assigned part

# Problem and task done
| Subsystem | Group |
| ---  | ---     |
| There is no problem occur| No assigned role for this week |


# Impact
- There is progress in report

# Next step
- Continue working on report



[ [Go to Week 10](Week 10.md) ][ [Go to Week 12](Week 12.md) ]
<br>
[ [Back to Main Page](Weekly_Report.md) ]
